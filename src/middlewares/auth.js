import axios from 'axios'
import config from '~/config'

// check that user is logged in before transfering to next
export function isAuth({ next, store }) {
    if (store.state.auth.isLogged) {
        return next()
    }
    return next({
        name: 'login'
    })
}
 
// ensure user is guest
export function isGuest({ next, store, router }) {
    if (store.state.auth.isLogged) {
        return next({
            name: 'monitor'
        })
    }
    return next()
}

// simulate token renewal by sending a post requet to api
export function renewToken() {
    let token
    //read and convert to object
    let storeObj = JSON.parse(localStorage.getItem('vuex'));
    if (storeObj && storeObj.auth) {
        token = storeObj.auth.token
    }
    let instance = axios
    instance = axios.create({
        baseURL: `${config.baseUrl}/orgs/${config.org_id}`,
        headers: {
            authorization: 'Bearer ' + token,
        }
    })
    instance.get(`/projects/`)
        .then(function(response) {
            // console.log(response);
        })
        .catch(function(error) {
            //  Invalid token, Not Authenticated.
            if (error.response && error.response.status === 401) {
                if (storeObj.auth) {
                    delete storeObj['auth']; //remove the key from object
                    delete storeObj['user'];
                    localStorage.setItem('vuex', JSON.stringify(storeObj));
                }
            }
        });
}
