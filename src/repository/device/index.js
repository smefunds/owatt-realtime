import request from '../axios.js'
import config from '~/config'


export default {
    async index() {
        return await request().get(`device/read`)
    },

    async store(data) {
        return await request().post(`device/store`, data, {
            'Content-Type': 'application/json'
        })
    },

    async find(imei) {
        return await request().get(`device/detail?imei=${imei}`)
    },

    async update(data) {
        return await request().patch(`device/update`, data)
    },

    async enable(data) {
        return await request().patch(`device/enable`, data)
        // {
        //     "imei":"202011000572"
        // }
    },

    async disable(data) {
        return await request().patch(`device/disable`, data)
    },

    async log(query) {
        // return await request().get(`inverter_logs?${query}`)
        console.log(config.baseUrl)
        const url = new URL(config.baseUrl + '/inverter_logs')
        url.search = new URLSearchParams(query)
        return await request().get(url)


        // {{iot_v2_base_url}}/inverter_logs?imei=24365056702418878107&from=2022-10-24 00:00:00&to=2022-10-24 23:50:50
    },

    async delete(data) {
         return await request().delete(`device/delete`, data)
    }
}


