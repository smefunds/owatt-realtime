import request from '../../axios.js'
import config from '~/config'

export default {
    async index(opts) {
        return await request().get(`admin/device_type/read`)
    },
    async store(data) {
        return await request().post(`admin/device_type/store`, data, {
            'Content-Type': 'application/json'
        })
    },
    async find(imei) {
        return await request().get(`admin/device_type/detail?imei=${imei}`)
    },
    async update(id, data) {
        return await request().patch(`admin/device_type/update`, data)
    },
    async delete(id) {
         return await request().patch(`admin/device_type/delete`)
    },

}


