import request from '../../axios.js'
import config from '~/config'


export default {
   
    async store(data) {
        return await request().post(`admin/device_list/store`, data, {
            'Content-Type': 'application/json'
        })
    },

}

