import request from '../../axios.js'
import config from '~/config'

export default {
    async index(opts) {
        return await request().get(`admin/device_list/read`)
    },
    async store(data) {
        return await request().post(`admin/device_list/store`, data, {
            'Content-Type': 'application/json'
        })
    },
    async find(imei) {
        return await request().get(`admin/device_list/detail?imei=${imei}`)
    },
    async update(id, data) {
        return await request().patch(`admin/device_list/update`, data)
    },
    async delete(id) {
         return await request().patch(`admin/device_list/delete`)
    },

    async getConsumption(query){
        const url = new URL(config.iotBaseUrl + 'admin/consumption/detail')
        url.search = new URLSearchParams(query)
        return await request().get(url)
    }
}


