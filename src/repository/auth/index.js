import request from '../axios.js'
export default {
  // Log in
  async login (data) {
    return await request().post('https://iot2.owattspay.net/api/v2/admin/login', data)

  },
  async user () {
    return await request().get('https://api.opensolar.com/api/fetch_token/')
  },
}
  

  // https://api.opensolar.com/api/fetch_token/