import request from '../axios.js'

export default {
    async index(opts) {
        return await request().get(`https://iot2.owattspay.net/api/v2/block/read`, 
            {
                headers: {
                    'token': 'Dxej1z3x.QTWfcxoITEF0u7A/IwkwHYBiqZkRdUbFsZ4uEGDttdFK'
                }
            }

        )
    },
    async store(data) {
        return await request().post(`https://iot2.owattspay.net/api/v2/block/store`, data, {
            'Content-Type': 'application/json',
            'token': 'Dxej1z3x.QTWfcxoITEF0u7A/IwkwHYBiqZkRdUbFsZ4uEGDttdFK'
        })
    },
    async find(imei) {
        return await request().get(`https://iot2.owattspay.net/api/v2/block/detail?reference=${imei}`,
            {
                headers: {
                    'token': 'Dxej1z3x.QTWfcxoITEF0u7A/IwkwHYBiqZkRdUbFsZ4uEGDttdFK'
                }
            }
        )
    },
    async update(id, data) {
        return await request().patch(``, data)
    },
    async delete(id) {
        return await request().delete('', data)
    }
}


