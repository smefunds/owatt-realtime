import request from '../axios.js'
import config from '~/config'


export default {
    async getConsumption(query){
        const url = new URL(config.baseUrl + '/consumption/detail')
        url.search = new URLSearchParams(query)
        return await request().get(url)
        // {{iot_v2_base_url}}/consumption/detail?imei=202011000510&report_type=1&year=2022
    },

    async getAllConsumption(query){
        const url = new URL(config.baseUrl + '/consumption/all_consumption')
        url.search = new URLSearchParams(query)
        return await request().get(url)
        // {{iot_v2_base_url}}/consumption/all_consumption?device_type=1&date=2022-10-15
    }
}
