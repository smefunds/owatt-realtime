import { createStore, createLogger } from 'vuex'
import user from './modules/auth/user'
import auth from './modules/auth'
import deviceadmin from './modules/device/admin'
import device from './modules/device/'
import block from './modules/block'

import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";
var ls = new SecureLS({ isCompression: false });


const debug = process.env.NODE_ENV !== 'production'

let plugins = [
	createPersistedState({

    paths: [
      'user', 
      'auth', 
      'deviceadmin',
      'device',
      'block', 
      ],

	// uncomment to implement encrypted localStorage
      // storage: {
      //   getItem: (key) => ls.get(key),
      //   setItem: (key, value) => ls.set(key, value),
      //   removeItem: (key) => ls.remove(key),
      // }
    }),
]

if(debug) plugins.push(createLogger())

const store = createStore({
    modules: {
      user,
      auth,
      deviceadmin,
      block,
      device,
    },
    strict: debug,
    plugins,
})

export default store
