import Block from '~/repository/block'
import { notify } from "~/plugins/notiwind/index.esm"
import { computed } from 'vue'


const state = () => ({
    blocks: '',
    block: '',
    validationErros: []
})
const getters = {
    getBlocks: (state) => {
        return state.blocks
    },
    getBlock: (state) => {
        return state.blocks
    }
}
const actions = {
    async fetchBlocks({ commit }, data) {
        await Block.index({page: 1, pageSize: 0})
            .then((res) => {
                if (res.data && res.status === 200) {
                    let blocks = res.data.data
                    commit('SET_BLOCKS', blocks)
                }
            })
    },

    async fetchBlock({ commit }, id) {
        await Block.find(id)
        .then((res)=>{
            if (res.data && res.status === 200) {
                let data = res.data
                data.data = JSON.parse(data.data)
                commit('SET_BLOCK', data)
            }
        })
    },

    async storeBlock({ commit, state }, data) {
        await Block.store(data)
            .then((res) => {
                if (res.data && res.status === 201) {
                    notify({
                      group: "success",
                      title: 'Success',
                      text: 'block added successfully'
                    }, 5000)
                }
            }).catch((e) => {
                if (e.response && e.response.status == 400) {
                    let errors = e.response.data
                    notify({
                        group: "error",
                        title: 'Validation Failed',
                        text: { ...errors }
                    }, 5000)
                }
            })
    },

    async updateBlock({commit, state}, id){
        let data = state.block
        block.update(id, data)
        .then(()=>{
            notify({
              group: "success",
              title: 'Updated',
              text: 'block updated successfully'
          }, 5000)
        })
    }
}
const mutations = {
    SET_BLOCKS(state, payload) {
        state.blocks = payload
    },
    SET_BLOCK(state, payload) {
        state.block = payload
    },
    SET_ERRORS(state, errors) {
        state.validationErro.push({ ...errors })
    }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
