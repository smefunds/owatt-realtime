import auth from '~/repository/auth'
const state = () => ({
    isLogged: false,
    token: '',
})

const getters = {
    isLogged: (state) => {
        return state.isLogged
    },
}

const actions = {
    async login({ commit, dispatch }, data) {

        let res = await auth.login(data)
        if (res.data.data.token && res.status === 200) {
            commit('setToken', res.data.data.token)
            dispatch('user/setUser', {...res.data.data} , { root:true })
            commit('setIsLogged', true)
            return res.data.data
        }


    },
    logout({ commit, dispatch }, data) {
        commit('setIsLogged', false)
        commit('setToken', '')
        dispatch('user/setUser', '', { root:true })
    },
}

const mutations = {
    setIsLogged (state, payload) {
        state.isLogged = payload
    },

    setToken (state, payload) {
        state.token = payload
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
