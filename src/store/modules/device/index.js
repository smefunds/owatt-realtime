import Device from '~/repository/device'
// import Block from '~/repository/block'
const state = () => ({
    devices: [],
    device: {},
    deviceData: {},
    currentDevice: {},
    currentBlock: {},
    deviceTypes: {},

})
const getters = {
    devices: (state) => {
        return state.devices
    },
    device: (state) => {
        return state.device
    },
    currentDevice: (state) => {
        return state.currentDevice
    },
    currentBlock: (state) => {
        return state.currentBlock
    },
    deviceTypes: (state) => {
        return state.deviceTypes
    }
}
const actions = {
    async fetchDevices({ commit }, data) {
        await Device.index()
            .then((res) => {
                if (res.data && res.status === 200) {
                    let devices = res.data.data
                    devices.map((device) => {
                        if (device.device_type == 1) {
                            device.name = 'Prepaid Meter'
                        }
                        if (device.device_type == 2) {
                            device.name = 'Kike Gas'
                        }
                        if (device.device_type == 3) {
                            device.name = 'SmartHome'
                        }
                        if (device.device_type == 4) {
                            device.name = 'Genstarter'
                        }
                        if (device.device_type == 5) {
                            device.name = 'AutoBidder'
                        }
                        if (device.device_type == 6) {
                            device.name = 'Gateway'
                        }
                        if (device.device_type == 7) {
                            device.name = 'Inverter'
                        }
                    })
                    commit('SET_DEVICES', devices)
                }
        })
    },
    async fetchDevice({ commit }, imei) {
        let res =  await Device.find(imei)
        return res.data.data
    },

    async fetchDeviceLogs({ commit }, query) {
        let res =  await Device.log(query)
        return res.data.data
    },
    
    async storeDevice({ commit, dispatch }, payload) {
        await Device.store(payload)
            .then((res) => {
                dispatch('fetchDevices')
            })
    },
    
}

const mutations = {
    SET_DEVICES(state, payload) {
        state.devices = payload
    },
    SET_DEVICE(state, payload) {
        state.device = payload
    },
    SET_CURRENT_DEVICE(state, payload) {
        state.currentDevice = payload
    },
    SET_CURRENT_BLOCK(state, payload) {
        state.currentBlock = payload
    },

    SET_DEVICE_TYPES(state, payload) {
        state.deviceTypes = payload
    },


}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
