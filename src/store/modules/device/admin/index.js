import DeviceType from '~/repository/device/admin/device_type'
import DeviceList from '~/repository/device/admin/device_list'
const state = () => ({
    deviceTypes: {},

})
const getters = {
    deviceTypes: (state) => {
        return state.deviceTypes
    }
}
const actions = {
    
    /*DEVICE TYPES*/
    async fetchDeviceTypes({ commit }, data) {
        await DeviceType.index()
        .then((res) => {
            if (res.data && res.status === 200) {
                commit('SET_DEVICE_TYPES', res.data.data)
            }
        })
    },

    async fetchDeviceType({  }, imei) {
        let res =  await DeviceType.find(imei)
        return res.data.data
    },
   
    async storeDeviceType({ dispatch }, payload) {
        await DeviceType.store(payload)
        .then((res) => {
            dispatch('fetchDeviceTypes')
        })
    },

    async addToDeviceList({ dispatch }, payload) {
        await DeviceList.store(payload)
        .then((res) => {
            dispatch('fetchDeviceTypes')
        })
    },
    
}

const mutations = {
    SET_DEVICES(state, payload) {
        state.devices = payload
    },
    SET_DEVICE(state, payload) {
        state.device = payload
    },
    SET_CURRENT_DEVICE(state, payload) {
        state.currentDevice = payload
    },
    SET_CURRENT_BLOCK(state, payload) {
        state.currentBlock = payload
    },

    SET_DEVICE_TYPES(state, payload) {
        state.deviceTypes = payload
    },


}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
